package com.maciej_witkowski.aplikacje_przemyslowe.config;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.AuthorRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.CommentRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostAuthorRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataConfiguration {

    private final AuthorRepository authorRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final PostAuthorRepository postAuthorRepository;

    public DataConfiguration(
            @Autowired AuthorRepository authorRepository,
            @Autowired PostRepository postRepository,
            @Autowired CommentRepository commentRepository,
            @Autowired PostAuthorRepository postAuthorRepository
    ) {
        this.authorRepository = authorRepository;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.postAuthorRepository = postAuthorRepository;
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            String[] beanNames = ctx.getBeanDefinitionNames();
            for (String beanName : beanNames) {
                Object bean = ctx.getBean(beanName);
                switch (bean.getClass().toString()) {
                    case "class com.maciej_witkowski.aplikacje_przemyslowe.model.Author":
                        System.out.println(bean);
                        authorRepository.addAuthor((Author) bean);
                        break;
                    case "class com.maciej_witkowski.aplikacje_przemyslowe.model.Post":
                        System.out.println(bean);
                        postRepository.addPost((Post) bean);
                        break;
                    case "class com.maciej_witkowski.aplikacje_przemyslowe.model.Comment":
                        System.out.println(bean);
                        commentRepository.addComment((Comment) bean);
                        break;
                    case "class com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor":
                        System.out.println(bean);
                        postAuthorRepository.addPostAuthor((PostAuthor) bean);
                        break;
                }
            }
        };
    }

}
