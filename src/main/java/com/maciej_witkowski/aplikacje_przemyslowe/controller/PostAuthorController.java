package com.maciej_witkowski.aplikacje_przemyslowe.controller;

import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/postAuthor")
public class PostAuthorController {

    private final PostAuthorRepository postAuthorRepository;

    public PostAuthorController(@Autowired PostAuthorRepository postAuthorRepository) {
        this.postAuthorRepository = postAuthorRepository;
    }

    @GetMapping
    ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(postAuthorRepository.getAllPostAuthors());
    }

    @GetMapping("/{id}")
    ResponseEntity<?> getByPostId(@PathVariable("id") int id, @RequestParam String type) {
        if (type.equals("post")) {
            return ResponseEntity.status(HttpStatus.OK).body(postAuthorRepository.getPostAuthorsByPostId(id));
        } else if (type.equals("author")) {
            return ResponseEntity.status(HttpStatus.OK).body(postAuthorRepository.getPostAuthorsByAuthorId(id));
        }
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
    }

}
