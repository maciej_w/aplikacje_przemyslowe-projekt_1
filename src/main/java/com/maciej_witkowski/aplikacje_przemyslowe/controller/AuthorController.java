package com.maciej_witkowski.aplikacje_przemyslowe.controller;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/author")
public class AuthorController {

    private final AuthorRepository authorRepository;

    public AuthorController(@Autowired AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping
    ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(authorRepository.getAllAuthors());
    }

    @PostMapping
    ResponseEntity<?> add(@RequestBody Author author) {
        try {
            authorRepository.addAuthor(author);
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(author);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable int id) {
        try {
            Author author = authorRepository.getAuthorById(id);
            return ResponseEntity.status(HttpStatus.FOUND).body(author);
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
        }
    }


}
