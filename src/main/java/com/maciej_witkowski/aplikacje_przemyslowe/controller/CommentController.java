package com.maciej_witkowski.aplikacje_przemyslowe.controller;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/comment")
public class CommentController {

    private final CommentRepository commentRepository;

    public CommentController(@Autowired CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @GetMapping
    ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(commentRepository.getAllComments());
    }

    @PostMapping
    ResponseEntity<?> add(@RequestBody Comment comment) {
        try {
            commentRepository.addComment(comment);
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(comment);
    }

}
