package com.maciej_witkowski.aplikacje_przemyslowe.controller;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.service.AuthorService;
import com.maciej_witkowski.aplikacje_przemyslowe.service.CommentService;
import com.maciej_witkowski.aplikacje_przemyslowe.service.PostAuthorService;
import com.maciej_witkowski.aplikacje_przemyslowe.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class PostController {

    @Autowired
    private PostService postService;

    @Autowired
    private PostAuthorService postAuthorService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private CommentService commentService;

    @GetMapping("/")
    public String home(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size
    ) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(25);

        Page<Post> posts = postService.findPaginated(PageRequest.of(currentPage - 1, pageSize));

        Random random = new Random();
        final String[] colors = {"primary", "success", "danger", "info", "dark"};

        model.addAttribute("posts", posts);
        model.addAttribute("colors", colors);
        model.addAttribute("random", random);

        int totalPages = posts.getTotalPages();

        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "home";
    }

    @GetMapping("/post/{id}")
    public String post(Model model, @PathVariable int id, @RequestParam("color") Optional<String> color) {
        String style = color.orElse("");
        Post post = postService.getPostById(id);
        List<Author> authors = postAuthorService.getAuthorsById(post.getId());
        List<Comment> comments = commentService.getAllByPostId(post.getId());

        model.addAttribute("post", post);
        model.addAttribute("authors", authors);
        model.addAttribute("comments", comments);
        model.addAttribute("color", style);

        return "post";
    }

    @GetMapping("/delete/{id}")
    public String deletePost(
            @PathVariable("id") int id,
            Model model) {
        postService.deletePost(id);
        return "redirect:/";
    }

    @GetMapping("/post/{postId}/delete/{commentId}")
    public String deleteComment(
            @PathVariable("postId") int postId,
            @PathVariable("commentId") int commentId,
            Model model) {
        commentService.deleteComment(commentId);
        return "redirect:/post/" + postId;
    }

    @GetMapping("/user/{username}")
    public String author(Model model, @PathVariable String username) {

        Random random = new Random();
        final String[] colors = {"primary", "success", "danger", "info", "dark"};

        model.addAttribute("colors", colors);
        model.addAttribute("random", random);

        Author author = authorService.getAuthorByUsername(username);
        Integer[] postIds = postAuthorService.getPostIdsByAuthorId(author.getId());
        List<Comment> comments = commentService.getAllByUsername(author.getUsername());
        List<Post> posts = new ArrayList<>();
        int postCommentsNum = 0;

        for (int id : postIds) {
            posts.add(postService.getPostById(id));
            postCommentsNum += commentService.getAllByPostId(id).size();
        }

        model.addAttribute("author", author);
        model.addAttribute("posts", posts);
        model.addAttribute("comments", comments);

        int tagsNum = posts.stream().map(Post::getTags).collect(Collectors.toList()).stream().mapToInt(x -> x.length).sum();
        double avgPostLength = posts.stream().map(Post::getContent).collect(Collectors.toList()).stream().mapToInt(String::length).average().orElse(0d);

        model.addAttribute("tagsNum", tagsNum);
        model.addAttribute("avgPostLength", avgPostLength);
        model.addAttribute("postCommentsNum", postCommentsNum);

        return "author";
    }

//    @PostMapping("/new")
//    ResponseEntity<?> add(@RequestBody Map<String, ?> data) {
//        try {
//            postRepository.addPost(data);
//        } catch (ResponseStatusException e) {
//            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
//        }
//        return ResponseEntity.status(HttpStatus.CREATED).body(data);
//    }
//
//    @PutMapping("{id}")
//    ResponseEntity<?> update(
//            @PathVariable("id") int id,
//            @RequestParam(required = false) String content,
//            @RequestParam(required = false) String[] tags,
//            @RequestParam(required = false) Integer[] authors
//    ) {
//        try {
//            Post post = postRepository.updatePost(id, content, tags, authors);
//            return ResponseEntity.status(HttpStatus.CREATED).body(post);
//        } catch (ResponseStatusException e) {
//            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
//        }
//    }
//
//    @DeleteMapping("{id}")
//    ResponseEntity<?> delete(@PathVariable int id) {
//        try {
//            postRepository.deletePost(id);
//        } catch (ResponseStatusException e) {
//            return ResponseEntity.status(e.getStatus()).body(e.getMessage());
//        }
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
//    }

}
