package com.maciej_witkowski.aplikacje_przemyslowe;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor;
import com.opencsv.CSVReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class Converter {

    public static void main(String[] args) throws Exception {

        final File catalog = new File("./src/main/resources/csv");
        List<String> files = new LinkedList<>();

        for (final File fileEntry : Objects.requireNonNull(catalog.listFiles())) {
            if (fileEntry.getName().contains(".csv")) {
                files.add(fileEntry.getPath());
            }
        }

        Collections.sort(files);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();

        Element beans = document.createElementNS("http://www.springframework.org/schema/beans", "beans");
        beans.setAttributeNS(
                "http://www.w3.org/2000/xmlns/",
                "xmlns:xsi",
                "http://www.w3.org/2001/XMLSchema-instance"
        );
        beans.setAttributeNS(
                "http://www.w3.org/2001/XMLSchema-instance",
                "xsi:schemaLocation",
                "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd"
        );

        document.appendChild(beans);

        for (String file : files) {

            List<List<String>> data = new ArrayList<>();
            String[] row;

            try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
                while ((row = csvReader.readNext()) != null) {
                    data.add(List.of(row));
                }
            }

            Class<?> currClass;

            if (data.get(0).equals(Arrays.asList("id", "first_name", "last_name", "username"))) {
                currClass = Author.class;
            } else if (data.get(0).equals(Arrays.asList("id", "post_content", "tags"))) {
                currClass = Post.class;
            } else if (data.get(0).equals(Arrays.asList("id", "username", "id_post", "comment_content"))) {
                currClass = Comment.class;
            } else if (data.get(0).equals(Arrays.asList("id_post", "id_author"))) {
                currClass = PostAuthor.class;
            } else {
                throw new Exception("Nie ma odpowiedniej klasy dla takich danych");
            }

            List<String> fields = Arrays.stream(currClass.getDeclaredFields()).map(Field::getName).collect(Collectors.toList());

            for (int i = 1; i < data.size(); i++) {
                Element bean = document.createElement("bean");
                beans.appendChild(bean);
                bean.setAttribute("class", currClass.getCanonicalName());
                for (int j = 0; j < fields.size(); j++) {
                    Element element = document.createElement("property");
                    bean.appendChild(element);
                    element.setAttribute("name", fields.get(j));
                    if (fields.get(j).equals("tags")) {
                        element.setAttribute("value", data.get(i).get(j).replaceAll(" ", ", "));
                    } else {
                        element.setAttribute("value", data.get(i).get(j));
                    }
                }
            }

        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File("./src/main/resources/beans.xml"));
        transformer.transform(domSource, streamResult);

    }

}
