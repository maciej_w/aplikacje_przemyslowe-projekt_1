package com.maciej_witkowski.aplikacje_przemyslowe.repository;


import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository {

    void addAuthor(Author author);
    List<Author> getAllAuthors();
    Author getAuthorById(int id);
    Author getAuthorByUsername(String username);

}
