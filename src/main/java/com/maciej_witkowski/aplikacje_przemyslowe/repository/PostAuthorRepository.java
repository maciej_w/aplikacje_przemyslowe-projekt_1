package com.maciej_witkowski.aplikacje_przemyslowe.repository;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostAuthorRepository {

    void deletePostAuthor(int postId, int authorId);
    void addPostAuthor(PostAuthor postAuthor);
    void addPostAuthor(int postId, int authorId);
    List<PostAuthor> getAllPostAuthors();
    List<PostAuthor> getPostAuthorsByPostId(int postId);
    List<PostAuthor> getPostAuthorsByAuthorId(int authorId);
    List<Author> getAuthorsById(int id);
    Integer[] getPostIdsByAuthorId(int authorId);
    Integer[] getAuthorIdsById(int id);


}
