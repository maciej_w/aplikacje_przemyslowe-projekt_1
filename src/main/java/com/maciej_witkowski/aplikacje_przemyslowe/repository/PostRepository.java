package com.maciej_witkowski.aplikacje_przemyslowe.repository;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface PostRepository {

    void deletePost(int id);
    Post updatePost(int id, String content, String[] tags, Integer[] authors);
    void addPost(Post post);
    void addPost(Map<String, ?> data);
    Post getPostById(int id);
    List<Post> getAllPosts();

}
