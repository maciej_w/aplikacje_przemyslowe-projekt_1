package com.maciej_witkowski.aplikacje_przemyslowe.repository;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {

    void deleteComment(int id);
    void addComment(Comment comment);
    List<Comment> getAllByUsername(String username);
    List<Comment> getAllByPostId(int postId);
    List<Comment> getAllComments();

}
