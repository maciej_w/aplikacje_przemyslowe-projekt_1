package com.maciej_witkowski.aplikacje_przemyslowe.model;

import lombok.Data;

@Data
public class Attachment {

    private int id;
    private int postId;
    private String filename;

}
