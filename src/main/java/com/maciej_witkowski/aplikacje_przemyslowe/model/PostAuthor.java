package com.maciej_witkowski.aplikacje_przemyslowe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostAuthor {

    private int postId;
    private int authorId;

}
