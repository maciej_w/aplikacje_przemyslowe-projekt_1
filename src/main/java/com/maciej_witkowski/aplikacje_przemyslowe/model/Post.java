package com.maciej_witkowski.aplikacje_przemyslowe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Post {

    private int id;
    private String content;
    private String[] tags;
    private LocalDateTime lastModified = LocalDateTime.now();

    public Post(int id, String content, String[] tags) {
        this.id = id;
        this.content = content;
        this.tags = tags;
    }
}