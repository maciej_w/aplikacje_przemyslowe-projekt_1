package com.maciej_witkowski.aplikacje_przemyslowe.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Comment {

    private int id;
    private String username;
    private int postId;
    private String body;
    private LocalDateTime lastModified = LocalDateTime.now();

    public Comment(int id, String username, int postId, String body) {
        this.id = id;
        this.username = username;
        this.postId = postId;
        this.body = body;
    }
}
