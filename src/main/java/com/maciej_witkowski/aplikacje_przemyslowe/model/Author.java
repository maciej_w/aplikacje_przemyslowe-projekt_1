package com.maciej_witkowski.aplikacje_przemyslowe.model;

import lombok.Data;

@Data
public class Author {

    private int id;
    private String firstName;
    private String lastName;
    private String username;

}
