package com.maciej_witkowski.aplikacje_przemyslowe.service;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostAuthorRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class PostService implements PostRepository {

    List<Post> repository = new ArrayList<>();
    private final PostAuthorRepository postAuthorRepository;

    public PostService(PostAuthorRepository postAuthorRepository) {
        this.postAuthorRepository = postAuthorRepository;
    }

    @Override
    public List<Post> getAllPosts() {
        return repository;
    }

    @Override
    public Post getPostById(int id) {
        Post post = repository.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
        if (post == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Post with given id does not exist!");
        }
        return post;
    }

    @Override
    public void addPost(Post post) {
        if (repository.stream().map(Post::getId).anyMatch(integer -> post.getId() == integer)) {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Post with given id already exists!");
        }
        repository.add(post);
    }

    @Override
    public void addPost(Map<String, ?> data) {
        try {
            @SuppressWarnings("unchecked")
            Post post = new Post((int) data.get("id"), (String) data.get("content"), ((ArrayList<String>) data.get("tags")).toArray(String[]::new));

            if (repository.stream().map(Post::getId).anyMatch(integer -> post.getId() == integer)) {
                throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Post with given id already exists!");
            }

            @SuppressWarnings("unchecked")
            ArrayList<Integer> authors = (ArrayList<Integer>) data.get("authors");

            repository.add(post);

            for (int authorId : authors) {
                postAuthorRepository.addPostAuthor(new PostAuthor(post.getId(), authorId));
            }

        } catch (ClassCastException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Given input is of the wrong type!");
        }
    }

    @Override
    public Post updatePost(int id, String content, String[] tags, Integer[] authors) {
        Post post = repository.stream().filter(x -> x.getId() == id).findFirst().orElse(null);

        if (post == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Post with given id does not exist!");
        }

        if (content != null && content.length() > 0 && !content.equals(post.getContent())) {
            post.setContent(content);
        }

        if (tags != null && tags.length > 0 && tags != post.getTags()) {
            post.setTags(tags);
        }

        if (authors != null && authors.length > 0) {
            Integer[] authorIds = postAuthorRepository.getAuthorIdsById(id);
            if (authorIds != authors) {
                for (int authorId : authorIds) {
                    postAuthorRepository.deletePostAuthor(id, authorId);
                }
                for (int authorId : authors) {
                    postAuthorRepository.addPostAuthor(id, authorId);
                }
            }
        }

        return post;
    }

    @Override
    public void deletePost(int id) {
        Post post = repository.stream().filter(x -> x.getId() == id).findFirst().orElse(null);

        if (post == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Post with given id does not exist!");
        }

        repository.remove(post);

        Integer[] authorIds = postAuthorRepository.getAuthorIdsById(id);

        for (int authorId : authorIds) {
            postAuthorRepository.deletePostAuthor(id, authorId);
        }
    }

    public Page<Post> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;

        List<Post> list;

        if (repository.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, repository.size());
            list = repository.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), repository.size());
    }

}
