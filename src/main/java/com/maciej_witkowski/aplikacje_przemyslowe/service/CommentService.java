package com.maciej_witkowski.aplikacje_przemyslowe.service;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Comment;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.CommentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommentService implements CommentRepository {

    List<Comment> repository = new ArrayList<>();

    @Override
    public List<Comment> getAllComments() {
        return repository;
    }

    @Override
    public List<Comment> getAllByPostId(int postId) {
        return repository.stream().filter(x -> x.getPostId() == postId).collect(Collectors.toList());
    }

    @Override
    public List<Comment> getAllByUsername(String username) {
        return repository.stream().filter(x -> Objects.equals(x.getUsername(), username)).collect(Collectors.toList());
    }

    @Override
    public void addComment(Comment comment) {
        if (repository.stream().map(Comment::getId).anyMatch(integer -> comment.getId() == integer)) {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Comment with given id already exists!");
        }
        repository.add(comment);
    }

    @Override
    public void deleteComment(int id) {
        Comment comment = repository.stream().filter(x -> x.getId() == id).findFirst().orElse(null);

        if (comment == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Comment with given id does not exist!");
        }

        repository.remove(comment);
    }

}
