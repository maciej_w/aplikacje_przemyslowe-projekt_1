package com.maciej_witkowski.aplikacje_przemyslowe.service;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.AuthorRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AuthorService implements AuthorRepository {

    List<Author> repository = new ArrayList<>();

    @Override
    public List<Author> getAllAuthors() {
        return repository;
    }

    @Override
    public void addAuthor(Author author) {
        if (repository.stream().map(Author::getUsername).anyMatch(author.getUsername()::equals)) {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Author with given username already exists!");
        } else if (repository.stream().map(Author::getId).anyMatch(integer -> author.getId() == integer)) {
            throw new ResponseStatusException(HttpStatus.SEE_OTHER, "Author with given id already exists!");
        }
        repository.add(author);

    }

    @Override
    public Author getAuthorById(int id) {
        Author author = repository.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
        if (author == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Author with given id does not exist!");
        }
        return author;
    }

    @Override
    public Author getAuthorByUsername(String username) {
        Author author = repository.stream().filter(x -> Objects.equals(x.getUsername(), username)).findFirst().orElse(null);
        if (author == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Author with given username does not exist!");
        }
        return author;
    }

}
