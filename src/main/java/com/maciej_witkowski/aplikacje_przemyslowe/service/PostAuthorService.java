package com.maciej_witkowski.aplikacje_przemyslowe.service;

import com.maciej_witkowski.aplikacje_przemyslowe.model.Author;
import com.maciej_witkowski.aplikacje_przemyslowe.model.Post;
import com.maciej_witkowski.aplikacje_przemyslowe.model.PostAuthor;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.AuthorRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostAuthorRepository;
import com.maciej_witkowski.aplikacje_przemyslowe.repository.PostRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostAuthorService implements PostAuthorRepository {

    List<PostAuthor> repository = new ArrayList<>();
    private final AuthorRepository authorRepository;

    public PostAuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<PostAuthor> getAllPostAuthors() {
        return repository;
    }

    @Override
    public List<Author> getAuthorsById(int id) {
        List<PostAuthor> postAuthors = repository.stream().filter(x -> x.getPostId() == id).collect(Collectors.toList());
        List<Author> authors = new ArrayList<>();

        for (PostAuthor author : postAuthors) {
            authors.add(authorRepository.getAuthorById(author.getAuthorId()));
        }

        return authors;
    }

    @Override
    public Integer[] getPostIdsByAuthorId(int authorId) {
        return repository.stream().filter(x -> x.getAuthorId() == authorId).map(PostAuthor::getPostId).toArray(Integer[]::new);
    }

    @Override
    public List<PostAuthor> getPostAuthorsByPostId(int postId) {
        return repository.stream().filter(x -> x.getPostId() == postId).collect(Collectors.toList());
    }

    @Override
    public List<PostAuthor> getPostAuthorsByAuthorId(int authorId) {
        return repository.stream().filter(x -> x.getAuthorId() == authorId).collect(Collectors.toList());
    }

    @Override
    public Integer[] getAuthorIdsById(int id) {
        return repository.stream().filter(x -> x.getPostId() == id).map(PostAuthor::getAuthorId).toArray(Integer[]::new);
    }

    @Override
    public void addPostAuthor(PostAuthor postAuthor) {
        if (repository.stream().anyMatch(
                x -> x.getAuthorId() == postAuthor.getAuthorId() && x.getPostId() == postAuthor.getPostId()
        )) {
            throw new ResponseStatusException(
                    HttpStatus.SEE_OTHER, "Post with the given id already contains this author!"
            );
        }
        if (authorRepository.getAllAuthors().stream().noneMatch(x -> x.getId() == postAuthor.getAuthorId())) {
            throw new ResponseStatusException(
                    HttpStatus.SEE_OTHER, "Author with given id does not exist!"
            );
        }
        repository.add(postAuthor);
    }

    @Override
    public void addPostAuthor(int postId, int authorId) {
        if (repository.stream().anyMatch(x -> x.getAuthorId() == postId && x.getPostId() == authorId)) {
            throw new ResponseStatusException(
                    HttpStatus.SEE_OTHER, "Post with the given id already contains this author!"
            );
        }
        if (authorRepository.getAllAuthors().stream().noneMatch(x -> x.getId() == authorId)) {
            throw new ResponseStatusException(
                    HttpStatus.SEE_OTHER, "Author with given id does not exist!"
            );
        }
        repository.add(new PostAuthor(postId, authorId));
    }

    @Override
    public void deletePostAuthor(int postId, int authorId) {
        PostAuthor postAuthor = repository.stream()
                .filter(x -> x.getPostId() == postId && x.getAuthorId() == authorId).findFirst().orElse(null);

        if (postAuthor == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Post with given id and author does not exist!");
        }

        repository.remove(postAuthor);
    }

}
